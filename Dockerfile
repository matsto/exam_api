FROM python:3.7.3
ENV PYTHONUNBUFFERED 1
MAINTAINER matsto
RUN mkdir /test_task1
WORKDIR /test_task1
ADD . /test_task1
RUN pip install -r requirements-prod.txt
EXPOSE 8000
CMD exec gunicorn test_task1.wsgi:application --bind 0.0.0.0:8000 --workers 3

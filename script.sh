HOST=127.0.0.1
PORT=8000

if [ $1 = "install" ]
then
    python3 -m venv venv
    . ./venv/bin/activate
    pip install -r ./requirements.txt
    python manage.py migrate
    python manage.py loaddata fixtures.json
    python manage.py runserver $HOST:$PORT
elif [ $1 = "run" ]
then
    python3 -m venv venv
    . ./venv/bin/activate
    python manage.py runserver $HOST:$PORT
elif [ $1 = "test" ]
then
    python3 -m venv venv
    . ./venv/bin/activate
    python manage.py test
elif [ $1 = "docker-run" ]
then
    sudo docker build --file=./Dockerfile  --tag=test_task1 ./
	sudo docker run  --name=test_task1 --publish=8000 test_task1
elif [ $1 = "isort" ]
then
    . ./venv/bin/activate
    pip install isort==4.3.17
    sh -c "isort --recursive . "
elif [ $1 = "lint" ]
then
    . ./venv/bin/activate
    pip install flake8==3.7.7
    flake8
elif [ $1 = "help" ]
then
    echo script.sh [OPTION]
    echo
    echo OPTIONS:
    echo
    echo install - installation of virtual environment, all requirements. After installation load fixtures and run a dev-server.
    echo run - starts virtual environment and a dev-server. It doesn\'t install virtual environment and requirements.
    echo docker-run - build and run docker container
    echo isort - run isort to clean-up imports in code
    echo lint - check your code if follow PEP8, etc.
fi

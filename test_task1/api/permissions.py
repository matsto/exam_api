from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):

    # Custom permission to only allow owners of an object to edit it.

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner
        return obj.owner == request.user


class IsRelatedToInstance(permissions.BasePermission):

    # Custom permission allows only people involved to get access to the instance
    # i.e. only particular student and teacher can look at student's results

    def has_object_permission(self, request, view, obj):
        if obj.student == request.user and request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner
        return obj.owner == request.user


class IsRelatedToStudentTask(permissions.BasePermission):

    # Custom permission allows only people involved to get access to the instance
    # i.e. only particular student and teacher can look at student's results

    def has_object_permission(self, request, view, obj):
        if obj.student_exam.student == request.user and request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner
        return obj.owner == request.user


class IsAllowedToView(permissions.DjangoModelPermissions):

    # Custom permission allows only people in groups with privileges to do more than GET

    def has_view_permission(self, request, obj):
        if request.user.groups is None:
            return False

from django.contrib.auth.models import User
from django.db import IntegrityError
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
# API tests
from rest_framework.test import APIClient, APIRequestFactory

from test_task1.api.models import Exam, StudentExam, StudentTask, Task
from test_task1.api.views import (ExamViewSet, GroupViewSet,
                                  StudentExamViewSet, StudentTaskViewSet,
                                  TaskViewSet, UserViewSet)

# MODEL TESTS

# common functions


def create_user(username='test', email='a22@p2.pl', password='Rosdsm123asd'):
    return User.objects.create(username=username, email=email, password=password)


def create_exam(user, name='exam', subject='subject', archived=False):
    return Exam.objects.create(owner=user, exam_name=name, exam_subject=subject, archived=archived)


def create_task(user, exam, name='test1', descr='test'):
    return Task.objects.create(owner=user, exam=exam, task_title=name, task_description=descr)


def create_student_exam(user, exam, student, grade=3):
    return StudentExam.objects.create(owner=user, exam=exam, student=student, exam_grade=grade)


def create_student_task(user, task, student_exam, answer='foolish', points=2):
    return StudentTask.objects.create(owner=user, task=task, student_exam=student_exam,
                                      answer=answer, task_points=points)

# tests


class TestModelUser(TestCase):

    # test django.auth.User model

    def test_register_model(self):
        # basic test
        self.assertTrue(isinstance(create_user(), User))

    def test_wrong_email(self):
        # test with wrong email (email is a string field, need to be validated through form)
        self.assertTrue(isinstance(create_user(email='wp2.pl'), User))

    def test_empty_username(self):
        # test with empty username
        self.assertTrue(isinstance(create_user(username=''), User))

    def test_empty_email(self):
        # test with empty email
        self.assertTrue(isinstance(create_user(email=''), User))

    def test_empty_password(self):
        # test with empty password
        self.assertTrue(isinstance(create_user(password=''), User))

    def test_same_username(self):
        # test with the same username
        create_user()
        with self.assertRaises(IntegrityError):
            create_user()


class TestModelExam(TestCase):

    # test model tests

    def test_exam(self):
        # basic tes of properly configured exam model instance
        user = create_user()
        self.assertTrue(isinstance(create_exam(user), Exam))

    def test_same_exam_name(self):
        # test with the same names of exams
        user = create_user()
        create_exam(user)
        with self.assertRaises(IntegrityError):
            create_exam(user)

    def test_empty_exam_name(self):
        # test with empty username
        user = create_user()
        with self.assertRaises(IntegrityError):

            create_exam(user, name=None)

    def test_empty_exam_subject(self):
        # test with empty username
        user = create_user()
        with self.assertRaises(IntegrityError):
            create_exam(user, subject=None)

    def test_empty_archived(self):
        # test with empty archived
        user = create_user()
        with self.assertRaises(IntegrityError):
            create_exam(user, archived=None)


class TestModelTask(TestCase):

    # test model task

    def test_task(self):
        # basic test of task
        user = create_user()
        exam = create_exam(user)
        self.assertTrue(isinstance(
                create_task(user, exam), Task)
        )

    def test_same_task_name(self):
        # test with the same names of tasks
        user = create_user()
        exam = create_exam(user)
        create_task(user, exam)
        with self.assertRaises(IntegrityError):
            create_task(user, exam)

    def test_empty_task_title(self):
        # test with task title
        user = create_user()
        exam = create_exam(user)
        with self.assertRaises(IntegrityError):
            create_task(user, exam, name=None)

    def test_empty_task_descript(self):
        # test with task description
        user = create_user()
        exam = create_exam(user)
        with self.assertRaises(IntegrityError):
            create_task(user, exam,  descr=None)


class TestModelStudentExam(TestCase):

    # test model student exam

    def test_student_exam(self):
        # basic test of student exam
        user = create_user()
        student = create_user('test1')
        exam = create_exam(user)
        self.assertTrue(isinstance(create_student_exam(user, exam, student), StudentExam))


class TestModelStudentTask(TestCase):

    # test model student task

    def test_student_task(self):
        # basic test of student exam
        user = create_user()
        exam = create_exam(user)
        task = create_task(user, exam)
        student = create_user('test1')
        student_exam = create_student_exam(user, exam, student)
        self.assertTrue(
            isinstance(create_student_task(user, task, student_exam), StudentTask))

# VIEW TESTS

class TestViewExam(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.client = APIClient()
        self.user = User.objects.create_user(
            username='jacob', email='jacob@gm.com',
            password='top_secret_pass_123', is_superuser=True
        )

# Tests of exam view

    def test_api_exam_get(self):
        # basic test if get request of exams return 200
        request = self.factory.get(reverse('exam-list'))
        request.user = self.user
        response = ExamViewSet.as_view({'get': 'list'})(request)
        self.assertEqual(response.status_code, 200)

    def test_api_exam_post(self):
        url = reverse('exam-list')
        data = {'exam_name': 'seriously looking name', 'exam_subject': 'Art', 'archived': 'false'}

        user = self.user

        self.client.login(username='jacob', password='top_secret_pass_123')
        response = self.client.post(url, data, format='json')

        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Exam.objects.count(), 1)
        self.assertEqual(Exam.objects.get().exam_name, 'seriously looking name')

    def test_api_get_an_exam(self):
        # Test the api can get a given exam.
        Exam.objects.create(owner=self.user, exam_name='name', exam_subject='subject', archived=False)

        exam = Exam.objects.get(exam_name='name')
        self.client.login(username='jacob', password='top_secret_pass_123')

        response = self.client.get(reverse('exam-detail', kwargs={'pk': exam.id}), format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

from django.contrib import admin

from test_task1.api.models import Exam, StudentExam, StudentTask, Task

# Register your models here.

admin.site.register(Exam)
admin.site.register(Task)
admin.site.register(StudentExam)
admin.site.register(StudentTask)

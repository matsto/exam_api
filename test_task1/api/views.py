from django.contrib.auth.models import Group, User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from test_task1.api.models import Exam, StudentExam, StudentTask, Task
from test_task1.api.permissions import (IsAllowedToView, IsOwnerOrReadOnly,
                                        IsRelatedToInstance,
                                        IsRelatedToStudentTask)
from test_task1.api.serializers import (ExamSerializer, GroupSerializer,
                                        StudentExamListSerializer,
                                        StudentExamSerializer,
                                        StudentTaskListSerializer,
                                        StudentTaskSerializer, TaskSerializer,
                                        UserListSerializer, UserSerializer)


class UserViewSet(viewsets.ModelViewSet):

    # the view presents the users trough api.

    # assigned permissions (description in api.permissions)
    permission_classes = (IsAuthenticated, IsAllowedToView)

    queryset = User.objects.all()
    serializer_class = UserSerializer

    # the view may be filtered with username or email
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('username', 'email')

    def list(self, request):
        queryset = User.objects.all()
        serializer = UserListSerializer(queryset, many=True)
        return Response(serializer.data)


class GroupViewSet(viewsets.ModelViewSet):

    # the view presents the groups trough api.

    # assigned permissions (description in api.permissions)
    permission_classes = (IsAuthenticated, IsAllowedToView)

    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    # the view may be filtered with name of the group
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('name',)


class ExamViewSet(viewsets.ModelViewSet):

    # the view presents the abstract (blank) exam sheets trough api.

    # assigned permissions (description in api.permissions)
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly, IsAllowedToView)

    queryset = Exam.objects.all()
    serializer_class = ExamSerializer

    # the view may be filtered with name of the teacher (owner), exam's name, exam's subject or check if it's archival
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('owner', 'exam_name', 'exam_subject', 'archived')

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class TaskViewSet(viewsets.ModelViewSet):

    # the view presents the abstract (blank) task trough api.

    # assigned permissions (description in api.permissions)
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly, IsAllowedToView)

    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    # the view may be filtered with name of the teacher (owner), exam's name, title of the task, or its description
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('owner', 'exam', 'task_title', 'task_description')

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class StudentExamViewSet(viewsets.ModelViewSet):

    # the view presents the real exam trough api.

    # assigned permissions (description in api.permissions)
    permission_classes = (IsAuthenticated, IsRelatedToInstance, IsAllowedToView)

    queryset = StudentExam.objects.all()
    serializer_class = StudentExamSerializer

    # the view may be filtered with name of the teacher (owner), exam's name, student's name and student's grade
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('owner', 'exam', 'student', 'exam_grade')

    def list(self, request):
        queryset = StudentExam.objects.all()
        serializer = StudentExamListSerializer(queryset, many=True)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class StudentTaskViewSet(viewsets.ModelViewSet):

    # the view presents the real task trough api.

    # assigned permissions (description in api.permissions)
    permission_classes = (IsAuthenticated, IsAllowedToView, IsRelatedToStudentTask)

    queryset = StudentTask.objects.all()
    serializer_class = StudentTaskSerializer

    # the view may be filtered with name of the teacher (owner), task's name, student's name and task's asset
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('owner', 'task', 'student_exam', 'task_points')

    def list(self, request):
        queryset = StudentTask.objects.all()
        serializer = StudentTaskListSerializer(queryset, many=True)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

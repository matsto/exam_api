from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Exam(models.Model):

    # Model of the abstract Exam, created by teacher, banned for students. Like prepared empty sheets in real world.

    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='exams')
    exam_name = models.CharField(max_length=100, blank=False, unique=True)
    exam_subject = models.CharField(max_length=50, blank=False)
    archived = models.BooleanField()

    def __str__(self):
        return "{} - {}".format(self.exam_name, self.exam_subject)


class Task(models.Model):

    # Model of the abstract task, created by teacher, banned for students.

    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tasks')
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE, related_name='tasks_exam')
    task_title = models.CharField(max_length=50, blank=False, unique=True)
    task_description = models.TextField(max_length=250, blank=False)

    def __str__(self):
        return "{} - {}".format(self.task_title, self.task_description)


class StudentExam(models.Model):

    # Model of real exam, written by student. Every student has instance of this,
    # but teacher is allowed to see and change them all.

    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='studentexams')
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE, related_name='exam')
    student = models.ForeignKey(User, on_delete=models.CASCADE)
    exam_grade = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(6)])

    def __str__(self):
        return "{} - {}".format(self.student, self.exam)


class StudentTask(models.Model):

    # Model of real task, written by student. Every student has instance of this,
    # but teacher is allowed to see and change them all.

    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='studenttasks')
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='task')
    student_exam = models.ForeignKey(StudentExam, on_delete=models.CASCADE)
    answer = models.CharField(max_length=50, blank=True)
    task_points = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(100)])

    def __str__(self):
        return "{} - {}".format(self.student_exam, self.task)

from django.urls import include, path
from rest_framework import routers

from test_task1.api.views import (ExamViewSet, GroupViewSet,
                                  StudentExamViewSet, StudentTaskViewSet,
                                  TaskViewSet, UserViewSet)

# routers to create in easy way structure of API

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'exams', ExamViewSet)
router.register(r'tasks', TaskViewSet)
router.register(r'student_exams', StudentExamViewSet)
router.register(r'student_tasks', StudentTaskViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include('rest_framework.urls')),
]

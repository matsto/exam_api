from django.contrib.auth.models import Group, User
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from test_task1.api.models import Exam, StudentExam, StudentTask, Task


class UserSerializer(serializers.ModelSerializer):

    # Serializer to handle User model of standard Django Auth

    class Meta:
        model = User
        fields = ('url', 'username', 'first_name', 'last_name',
                  'email', 'groups', 'last_login', 'date_joined')
        read_only_fields = ('last_login', 'date_joined')


class UserListSerializer(serializers.ModelSerializer):

    # Serializer to handle User model in list view and hide sensitive data

    class Meta:
        model = User
        fields = ('id', 'username', 'groups')


class GroupSerializer(serializers.ModelSerializer):

    # Serializer to handle Group model of standard Django Auth

    class Meta:
        model = Group
        fields = '__all__'


class ExamSerializer(serializers.HyperlinkedModelSerializer):

    # Serializer to handle abstract Exams by teacher, create/edit/del instances

    class Meta:
        model = Exam
        fields = '__all__'
        read_only_fields = ('owner',)


class TaskSerializer(serializers.HyperlinkedModelSerializer):

    # Serializer to handle abstract Task by teacher, create/edit/del instances

    class Meta:
        model = Task
        fields = '__all__'
        read_only_fields = ('owner',)


class StudentExamSerializer(serializers.ModelSerializer):

    # Serializer to handle real Exams by teacher and student, create/edit/del instances to which they are involved in

    class Meta:
        model = StudentExam
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=StudentExam.objects.all(),
                fields=('student', 'exam')
            )
        ]
        read_only_fields = ('owner',)


class StudentExamListSerializer(serializers.ModelSerializer):

    # Serializer for list, similar to above one, hide sensitive data - grade
    class Meta:
        model = StudentExam
        fields = ('id', 'owner', 'exam', 'student')
        read_only_fields = ('owner',)


class StudentTaskSerializer(serializers.ModelSerializer):

    # Serializer to handle real task by teacher and student, create/edit/del instances to which they are involved in

    def validate(self, data):
        # check if data from serializer is present in the data base, if so raise an error

        student_exam = data['student_exam']
        task = data['task']

        # check if user picked correct task and student exam, compare roots (exam)
        # and raise an error is they don't match

        # check what is the root exam of the task
        query_task = task.exam

        # check what is the root exam of the student exam
        query_exam = student_exam.exam

        # compare is they are the same save to database

        if query_task != query_exam:
            raise ValueError('A "{}" does not belong to "{}" exam. Pick correct values. "{}" belongs to "{}" exam.'
                             .format(task, query_exam, task, query_task))

    class Meta:
        model = StudentTask
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=StudentTask.objects.all(),
                fields=('task', 'student_exam')
            )
        ]
        read_only_fields = ('owner',)


class StudentTaskListSerializer(serializers.ModelSerializer):

    # Serializer for list, similar to above one, hide sensitive data like grade, answer

    class Meta:
        model = StudentTask
        fields = ('id', 'owner', 'task', 'student_exam')
        read_only_fields = ('owner',)

Readme dated to 19.04.2019

Test_task1

This application is an answer on asked question. It runs Django app with REST API to manage and evaluate exams.
The API is divided on two sections one to supply a teacher possibility to create exam and task(s), second is common for teachers and students, instances of filled-up exams, ready to evaluated are managed by this part (part is prefixed “student-*”).
The API has some permissions out of the box to prevent such a situations like students with access to another students filled-up exams. Other secured situation is student’s access to teachers exams/tasks. Very limited (just view of lists) is access for people without group.
To manage group privileges use group API.
There is no possibility to create new user with API because wrong interpretation of password field (it’s saved in plain text). To create new user please use admin panel.
Every existing user has password set as: `t3stpass!@#`.

List of existing users:
* `admin`
* `teacher`
* `student_1`
* `student_2`

How to:
* `script.sh install` – To run the application for the very first time. It will install and run everything for you, just open `127.0.0.1:8000` in your favorite browser.
* `script.sh run` – This is basic method to run the app once it’s installed.
* `script.sh test` – to run unit tests
* `script.sh help` – present you another options

In the app there is hard-coded `SECURITY_KEY`,  for test purposes it is acceptable.
